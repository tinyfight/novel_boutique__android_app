package com.xiong.itome.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
/**
 * 不能左右滑动的ViewPager
 * @author 熊小洋
 *
 */
public class NoScrollViewPager extends ViewPager {

	public NoScrollViewPager(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public NoScrollViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	@Override
	public boolean onInterceptTouchEvent(MotionEvent arg0) {
		
		return false;//触摸事件能够往下传递
	}
	@Override
	public boolean onTouchEvent(MotionEvent arg0) {
		
		return false;//触摸事件无响应
	}

}
